# My website - PT1


## Description
Un site web réalisé dans le cadre de mes études en DUT informatique (1ère année), lors d'un projet tutoré.
Les deux premières pages ont été imposées (Accueil et Ambition), tandis que le sujet de la troisième page étais libre.

## Utilisation
Pour parcourir ce site, il vous suffit de télécharger le projet et d'ouvrir le ficher Index.html.
